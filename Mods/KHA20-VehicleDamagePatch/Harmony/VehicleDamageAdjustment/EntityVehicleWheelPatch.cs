﻿using DMT;
using HarmonyLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using UnityEngine;

public class VehicleOnDamagePatch_Init
{
    [HarmonyPatch(typeof(EntityVehicle))]
    [HarmonyPatch("OnCollisionForward")]
    public class Khaine_OnCollisionForward_Patch
    {
        // Loops around the instructions and removes the return condition.
        static IEnumerable<CodeInstruction> Transpiler(IEnumerable<CodeInstruction> instructions)
        {
            UnityEngine.Debug.Log("Patching OnCollisionForward ()");

            // Grab all the instructions
            var codes = new List<CodeInstruction>(instructions);
            for (int i = 0; i < codes.Count; i++)
            {			
                if (codes[i].opcode == OpCodes.Ldc_R4 && (float)codes[i].operand == (float)0.8)
                {
                    UnityEngine.Debug.Log("Patching...");
                    codes[i].operand = (float)10;
					UnityEngine.Debug.Log("Adjusting 0.8 to 10");
					break;
                }
            }
            UnityEngine.Debug.Log("Done With Patching OnCollisionForward ()");

            return codes.AsEnumerable();
        }
    }
}
