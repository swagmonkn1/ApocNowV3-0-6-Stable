using DMT;
using HarmonyLib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Xml;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using UnityEngine;

public class DisableTraderProtection_Patch
{
	public class DisableTraderProtection_Init : IModApi
    {
		public void InitMod(Mod _modInstance)
		{
			Debug.Log(" Loading Patch: " + this.GetType().ToString());

			// Reduce extra logging stuff
			Application.SetStackTraceLogType(UnityEngine.LogType.Log, StackTraceLogType.None);
			Application.SetStackTraceLogType(UnityEngine.LogType.Warning, StackTraceLogType.None);

			var harmony = new HarmonyLib.Harmony(GetType().ToString());
			harmony.PatchAll(Assembly.GetExecutingAssembly());
		}
    }
	/*
    [HarmonyPatch(typeof(TraderArea), MethodType.Constructor)]
    [HarmonyPatch("TraderArea")]
	public class TraderArea_Patch
    {				
		public static void Postfix(TraderArea __instance, Vector3i _pos, Vector3i _size, Vector3i _protectSize, Vector3i _teleportCenter, Vector3i _teleportSize)
		{
			int x = 0;
			int y = 0;
			int z = 0;
			
			__instance.ProtectSize = new Vector3i(x, y, z);
		}
	}
	*/
        [HarmonyPatch(typeof(TraderArea), MethodType.Constructor)]
        [HarmonyPatch(new Type[]
        {
            typeof(Vector3i),
            typeof(Vector3i),
            typeof(Vector3i),
            typeof(Vector3i),
            typeof(Vector3i)
        })]
        public class TraderArea_Patch
        {
            public static void Postfix(TraderArea __instance)
            {
                Vector3i vector3I = new Vector3i(0, 0, 0);
                __instance.ProtectSize = vector3I;
            }
        }
}